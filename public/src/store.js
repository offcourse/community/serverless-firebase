import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state=  {
  selectedCheckpoint: false,
  selectedLanguage: "en"
};

const mutations = {
  selectCheckpoint (state, checkpointId) {
    state.selectedCheckpoint = checkpointId;
  },
  selectLanguage (state, language) {
    state.selectedLanguage = language;
  }
};

export default new Vuex.Store({
  state,
  mutations
});
