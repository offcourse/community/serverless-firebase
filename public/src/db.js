var firebase = require('firebase');
import VueFire from 'vuefire';
import Vue from 'vue';

Vue.use(VueFire);

var config = {
  apiKey: "AIzaSyDsUd3HHj3J9pl3wSIG_eWr9QgefhN1BfE",
  authDomain: "serverless-meetup.firebaseapp.com",
  databaseURL: "https://serverless-meetup.firebaseio.com",
  projectId: "serverless-meetup",
  storageBucket: "serverless-meetup.appspot.com",
  messagingSenderId: "245060867663"
};

const firebaseApp = firebase.initializeApp(config);
const db  = firebaseApp.database();
const courseRef = db.ref('courses/serverless-meetup');
export { firebase, db, courseRef};
