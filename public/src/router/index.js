import Vue from 'vue';
import VueRouter from 'vue-router';
import Admin from '../components/Admin.vue';
import Main from '../components/Main.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Main },
  { path: '/admin', component: Admin }
];

export default new VueRouter({
  mode: 'history',
  routes
});
